#!/bin/bash

# Create directories
mkdir keys data &> /dev/null

# Get the lastest node version
git clone -b develop https://github.com/OriginTrail/ot-node.git
cd ot-node

node register-node.js | tee -a complete-node.log
